#!/usr/bin/env nu

print $'            (ansi yb)($env.USER)(ansi reset)@(ansi yb)((sys).host.hostname)(ansi reset)
  (ansi yb)|\   |(ansi reset)    (ansi y)os(ansi reset)    ((sys).host.name) ((sys).host.os_version)
  (ansi yb)| \  |(ansi reset)    (ansi y)kver(ansi reset)  ($nu.os-info.kernel_version)
  (ansi yb)|  \ |(ansi reset)    (ansi y)up(ansi reset)    ((sys).host.uptime)
  (ansi yb)|   \|(ansi reset)    (ansi y)sh(ansi reset)    ($env.SHELL | split row '/' | last)
            (ansi y)mem(ansi reset)   ((sys).mem.used) / ((sys).mem.total)'
