#!/usr/bin/env nu

let os = (open /etc/os-release | find PRETTY_NAME | parse "{a}="{b}"{c}" | get b.0)

let uptime_duration = (sys).host.uptime
let uptime = ((if $uptime_duration < 1min {
  $uptime_duration
} else {
  $"($uptime_duration)" | str replace " ..?sec" ""
})
  | str replace 'day' 'd'
  | str replace 'hr' 'h'
  | str replace 'min' 'm'
  | str replace 'sec' 's'
)

let mem = ($"((sys).mem.used | format filesize MiB) / ((sys).mem.total | format filesize MiB)"
  | str replace -a '\.' ''
  | str replace -a ' MiB' 'M')

print $'            (ansi yb)($env.USER)(ansi reset)@(ansi yb)((sys).host.hostname)(ansi reset)
  (ansi yb)|\   |(ansi reset)    (ansi y)os(ansi reset)    ((sys).host.name) ((sys).host.os_version)
  (ansi yb)| \  |(ansi reset)    (ansi y)kver(ansi reset)  ($nu.os-info.kernel_version)
  (ansi yb)|  \ |(ansi reset)    (ansi y)up(ansi reset)    ($uptime)
  (ansi yb)|   \|(ansi reset)    (ansi y)sh(ansi reset)    ($env.SHELL | split row '/' | last)
            (ansi y)mem(ansi reset)   ($mem)'
